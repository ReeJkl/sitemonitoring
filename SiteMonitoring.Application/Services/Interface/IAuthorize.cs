﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteMonitoring.Application.Services.Interface
{
    public interface IAuthorize
    {
        bool Auth(string login, string password);
    }
}
