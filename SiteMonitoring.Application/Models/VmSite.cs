﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SiteMonitoring.Application.Models
{
    public class VmSite
    {
        [Key]
        public Guid SiteId { get; set; }

        /// <summary>
        /// Имя и возраст
        /// </summary>
        [Required]
        [Display(Name = "Сайт")]
        public string Name { get; set; }

        /// <summary>
        /// Должность
        /// </summary>
        /// [Required]
        [Display(Name = "Описание")]
        public string Description { get; set; }

        /// <summary>
        /// Город
        /// </summary>
        [Required]
        [Display(Name = "Время обновления")]
        public TimeSpan Time { get; set; }

        /// <summary>
        /// Город
        /// </summary>
        [Display(Name = "Статус")]
        public bool Status { get; set; }
    }
}
