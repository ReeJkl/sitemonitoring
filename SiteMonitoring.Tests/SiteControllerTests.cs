using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Quartz;
using SiteMonitoring.Application.Controllers;
using SiteMonitoring.Application.Models;
using SiteMonitoring.Application.Services.Interface;
using SiteMonitoring.Database.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace SiteMonitoring.Tests
{
    public class SiteControllerTests
    {
        [Fact]
        public void IndexReturnsAViewResultWithAListOfSites()
        {
            // Arrange
            var mockRepo = new Mock<IRepository<Site>>();
            var mockMapper = new Mock<IMapper>();
            mockRepo.Setup(repo => repo.GetAll()).Returns(GetTestSites());
            var controller = new SiteController(mockMapper.Object, mockRepo.Object);

            // Act
            var result = controller.Index();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<VmSite>>(viewResult.Model);
            Assert.Equal(GetTestSites().Count(), model.Count());
        }

        [Fact]
        public void AddSiteWithSuccessRedirect()
        {
            // Arrange
            var newSite = new VmSite() { Name = "name1", Time = TimeSpan.FromMinutes(10) };
            var newSiteEntity = new Site() { SiteId = Guid.NewGuid(), Name = "name1", Time = TimeSpan.MinValue };
            var mockRepo = new Mock<IRepository<Site>>();
            var mockMapper = new Mock<IMapper>();
            var mockSheduler = new Mock<IScheduler>();

            var controller = new SiteController(mockMapper.Object, mockRepo.Object);

            // Act
            var result = controller.Create(mockSheduler.Object, newSite);

            // Assert
            var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result.Result);
            Assert.Equal("Index", redirectToActionResult.ActionName);
        }

        [Fact]
        public void AddSiteWithFailure()
        {
            // Arrange
            var mockRepo = new Mock<IRepository<Site>>();
            var mockMapper = new Mock<IMapper>();
            var mockSheduler = new Mock<IScheduler>();
            var controller = new SiteController(mockMapper.Object, mockRepo.Object);
            controller.ModelState.AddModelError("Name", "Required");
            var newSite = new VmSite();

            // Act
            var result = controller.Create(mockSheduler.Object, newSite);

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result.Result);
            Assert.Equal(newSite, viewResult?.Model);
        }

        [Fact]
        public void DeleteWithSuccessRedirect()
        {
            // Arrange
            var newSite = new VmSite() { Name = "name1", Time = TimeSpan.FromMinutes(10) };
            var newSiteEntity = new Site() { SiteId = Guid.NewGuid(), Name = "name1", Time = TimeSpan.MinValue };
            var mockRepo = new Mock<IRepository<Site>>();
            mockRepo.Setup(x => x.Delete(It.IsAny<Site>())).Verifiable();
            var mockMapper = new Mock<IMapper>();

            var controller = new SiteController(mockMapper.Object, mockRepo.Object);

            // Act
            var result = controller.Delete(newSite);

            // Assert
            var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("Index", redirectToActionResult.ActionName);
            mockRepo.Verify(r => r.Delete(It.IsAny<Site>()), Times.Once);
        }

        private IQueryable<Site> GetTestSites()
        {
            var sites = new List<Site>
            {
                new Site() { SiteId=Guid.NewGuid(),Name="name1",Description="",Status=true,Time=TimeSpan.MinValue},
                new Site() { SiteId=Guid.NewGuid(),Name="name2",Description="",Status=true,Time=TimeSpan.MinValue},
                new Site() { SiteId=Guid.NewGuid(),Name="name3",Description="",Status=true,Time=TimeSpan.MinValue},
                new Site() { SiteId=Guid.NewGuid(),Name="name4",Description="",Status=true,Time=TimeSpan.MinValue},
            }.AsQueryable();
            return sites;
        }
    }
}
