﻿using Quartz;
using SiteMonitoring.Application.Services.Interface;
using SiteMonitoring.Database;
using SiteMonitoring.Database.Entities;
using System;
using System.Net.NetworkInformation;
using System.Threading.Tasks;

namespace SiteMonitoring.Application.Jobs
{
    public class AvailableCheckJob : IJob
    {
        SMDbContext _repository;

        public AvailableCheckJob(SMDbContext repository)
        {
            _repository = repository;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            JobDataMap dataMap = context.JobDetail.JobDataMap;

            var name = dataMap.GetString("modelName");
            var id = Guid.Parse(dataMap.GetString("modelId"));

            Ping ping = new Ping();
            PingReply pingReply = null;
            pingReply = await ping.SendPingAsync(name);

            var entity = await _repository.Sites.FindAsync(id);

            if (pingReply.Status != IPStatus.TimedOut)
            {
                entity.Status = true;
                _repository.Update(entity);
                _repository.SaveChanges();
            }
            else
            {
                entity.Status = false;
                _repository.Update(entity);
                _repository.SaveChanges();
            }
        }
    }
}
