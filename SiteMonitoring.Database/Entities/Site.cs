﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SiteMonitoring.Database.Entities
{
    public class Site
    {
        [Key]
        public Guid SiteId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public TimeSpan Time { get; set; }

        public bool Status { get; set; }
    }
}
