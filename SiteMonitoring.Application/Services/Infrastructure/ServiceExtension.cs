﻿using Microsoft.Extensions.DependencyInjection;
using Quartz.Spi;
using SiteMonitoring.Application.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteMonitoring.Application.Services.Infrastructure
{
    public static class ServiceExtension
    {
        public static void AddIoCServices(this IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient<IAuthorize, SimpleAuthorize>();
        }
    }
}
