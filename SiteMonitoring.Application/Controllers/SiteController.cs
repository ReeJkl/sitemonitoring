﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Quartz;
using SiteMonitoring.Application.Jobs;
using SiteMonitoring.Application.Models;
using SiteMonitoring.Application.Services.Infrastructure;
using SiteMonitoring.Application.Services.Interface;
using SiteMonitoring.Database.Entities;

namespace SiteMonitoring.Application.Controllers
{
    public class SiteController : Controller
    {
        readonly IMapper _mapper;
        IRepository<Site> _repository;
        public SiteController(IMapper mapper, IRepository<Site> repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public ActionResult Index()
        {
            var list = _repository.GetAll();
            var models = list.Select(x => _mapper.Map<VmSite>(x)).ToList();
            return View(models);
        }

        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([FromServices]IScheduler scheduler, VmSite model)
        {
            if (ModelState.IsValid)
            {
                model.Status = false;
                var entity = _mapper.Map<Site>(model);
                var guid = await _repository.AddAsync(entity);
                QuartzExtension.StartJob(scheduler, model.Time,
                    JobBuilder.Create<AvailableCheckJob>()
                    .UsingJobData("modelId", guid.ToString())
                    .UsingJobData("modelName", model.Name)
                    .Build());
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        [Authorize]
        public async Task<ActionResult> Delete(Guid id)
        {
            var entity = await _repository.GetAsync(id);
            var model = _mapper.Map<VmSite>(entity);
            return View(model);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(VmSite model)
        {
            var entity = _mapper.Map<Site>(model);
            _repository.Delete(entity);
            return RedirectToAction(nameof(Index));
        }
    }
}