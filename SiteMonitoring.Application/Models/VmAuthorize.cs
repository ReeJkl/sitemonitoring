﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteMonitoring.Application.Models
{
    public class VmAuthorize
    {
        public string Login { get; set; }

        public string Password { get; set; }
    }
}
