﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SiteMonitoring.Application.Services.Interface;
using SiteMonitoring.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SiteMonitoring.Application.Services
{
    public class Repository<T> : IRepository<T> where T : class
    {
        SMDbContext db;

        public Repository(SMDbContext context)
        {
            db = context;
        }

        public async Task<T> GetAsync(Guid id)
        {
            return await db.FindAsync<T>(id);
        }

        public IQueryable<T> GetAll()
        {
            return db.Set<T>().AsQueryable();
        }

        public async Task<Guid> AddAsync(T entity)
        {
            var keyValue = typeof(T).GetProperties()
                .FirstOrDefault(x => x.CustomAttributes
                .Any(z => z.AttributeType == typeof(KeyAttribute)));
            keyValue.SetValue(entity, Guid.NewGuid());
            await db.AddAsync(entity);
            await db.SaveChangesAsync();
            return (Guid)keyValue.GetValue(entity);
        }

        public void Delete(T entity)
        {
            db.Remove(entity);
            db.SaveChanges();
        }

        public void Update(T entity)
        {
            db.Update(entity);
            db.SaveChanges();
        }
    }
}
