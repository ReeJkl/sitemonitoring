﻿using Microsoft.EntityFrameworkCore;
using SiteMonitoring.Database.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SiteMonitoring.Database
{
    public class SMDbContext : DbContext
    {
        public DbSet<Site> Sites { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("User ID=postgres;Password=321;Host=localhost;Port=5432;Database=SiteMonitoring;Pooling=true;");
            optionsBuilder.EnableSensitiveDataLogging();
        }
    }
}
