﻿using AutoMapper;
using SiteMonitoring.Application.Models;
using SiteMonitoring.Database.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteMonitoring.Application.Services.Infrastructure
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Site, VmSite>();
            CreateMap<VmSite, Site>();
        }
    }
}
