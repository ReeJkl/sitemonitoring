﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteMonitoring.Application.Services.Interface
{
    public interface IRepository<T>
    {
        Task<Guid> AddAsync(T entity);
        void Delete(T entity);
        Task<T> GetAsync(Guid id);
        IQueryable<T> GetAll();
    }
}
