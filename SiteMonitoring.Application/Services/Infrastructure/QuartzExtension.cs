﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using SiteMonitoring.Application.Jobs;
using SiteMonitoring.Database;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;

namespace SiteMonitoring.Application.Services.Infrastructure
{
    internal static class QuartzExtension
    {
        public static void StartJob(IScheduler scheduler, TimeSpan runInterval, IJobDetail job)
        {
            var trigger = TriggerBuilder.Create()
                .StartNow()
                .WithSimpleSchedule(scheduleBuilder =>
                    scheduleBuilder
                        .WithInterval(runInterval)
                        .RepeatForever())
                .Build();

            scheduler.ScheduleJob(job, trigger);
        }

        public static void UseQuartz(this IServiceCollection services, Type job)
        {
            services.AddSingleton<IJobFactory, CheckJobFactory>();
            services.Add(new ServiceDescriptor(job, job, ServiceLifetime.Transient));

            services.AddSingleton(provider =>
            {
                var schedulerFactory = new StdSchedulerFactory();
                var scheduler = new StdSchedulerFactory().GetScheduler();
                scheduler.Wait();
                scheduler.Result.JobFactory = provider.GetService<IJobFactory>();
                scheduler.Result.Start();
                return scheduler.Result;
            });
        }

        public async static void StartRecentJobs(this IApplicationBuilder app)
        {
            var scheduler = (IScheduler)app.ApplicationServices.GetService(typeof(IScheduler));
            var db = app.ApplicationServices.GetService<SMDbContext>();
            var sites = db.Sites;
            foreach(var site in sites)
            {
                StartJob(scheduler, site.Time,
                    JobBuilder.Create<AvailableCheckJob>()
                    .UsingJobData("modelId", site.SiteId.ToString())
                    .UsingJobData("modelName", site.Name)
                    .Build());
            }
        }
    }
}
