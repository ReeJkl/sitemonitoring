﻿using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using SiteMonitoring.Application.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteMonitoring.Application.Services.Infrastructure
{
    internal class Quartz
    {
        private IScheduler _scheduler;

        public static IScheduler Scheduler { get { return Instance._scheduler; } }

        private static Quartz _instance = null;

        public static Quartz Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Quartz();
                }
                return _instance;
            }
        }

        private Quartz()
        {
            Init();
        }

        private async void Init()
        {
            _scheduler = await new StdSchedulerFactory().GetScheduler();
        }

        public IScheduler UseJobFactory()
        {
            var services = new ServiceCollection();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            var container = services.BuildServiceProvider();
            var jobFactory = new CheckJobFactory(container);
            Scheduler.JobFactory = jobFactory;
            return Scheduler;
        }

        public async void AddJob(TimeSpan interval, IJobDetail job)
        {
            ITrigger jobTrigger = TriggerBuilder.Create()
                .StartNow()
                .WithSimpleSchedule(t => t.WithInterval(interval)
                .RepeatForever())
                .Build();

            await Scheduler.ScheduleJob(job, jobTrigger);
        }

        public static async void Start()
        {
            await Scheduler.Start();
        }
    }
}
